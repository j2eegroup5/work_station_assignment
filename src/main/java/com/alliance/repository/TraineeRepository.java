package com.alliance.repository;

import com.alliance.entity.Trainee;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository()
public interface TraineeRepository extends JpaRepository<Trainee, Long>
{
	//find by id. findOneById is a predefined. Should not be changed.
	public Trainee findOneById(Integer TraineeID);
}