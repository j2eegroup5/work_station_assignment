package com.alliance.repository;

import com.alliance.entity.Screen;
import com.alliance.repository.custom.ScreenRepositoryCustom;
import com.alliance.repository.custom.GenericRepositoryCustom;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("screenRepository")
public interface ScreenRepository extends JpaRepository<Screen, Integer>, ScreenRepositoryCustom, GenericRepositoryCustom {
	public Screen findOneById(Integer screenId);
}
