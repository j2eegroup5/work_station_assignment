package com.alliance.repository;

import com.alliance.entity.Mouse;
import com.alliance.repository.custom.MouseRepositoryCustom;
import com.alliance.repository.custom.GenericRepositoryCustom;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("mouseRepository")
public interface MouseRepository extends JpaRepository<Mouse, Integer>, MouseRepositoryCustom, GenericRepositoryCustom {
	public Mouse findOneById(Integer mouseId);
}
