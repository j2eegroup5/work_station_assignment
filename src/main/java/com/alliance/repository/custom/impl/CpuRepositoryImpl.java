package com.alliance.repository.custom.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.SQLQuery;
import org.hibernate.type.LongType;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.alliance.entity.Cpu;
import com.alliance.repository.custom.CpuRepositoryCustom;
import com.alliance.utility.StringUtility;

@Repository("cpuRepositoryCustom")
@Transactional(readOnly=true)
public class CpuRepositoryImpl extends GenericRepositoryImpl implements CpuRepositoryCustom{
	
	@PersistenceContext
	EntityManager entityManager;

	@Override
	public List<Cpu> listCpus(Cpu searchFilter) {
		// TODO Auto-generated method stub
		StringBuilder sqlQuery = new StringBuilder("SELECT * FROM cpu cpu ");
		if(!StringUtility.isStringNullOrEmpty(searchFilter.getCpu_brand())) {
			sqlQuery.append(" WHERE cpu.cpu_brand LIKE :cpu_brand ");
		}
		
		Query query = entityManager.createNativeQuery(sqlQuery.toString(), Cpu.class);
		
		if(!StringUtility.isStringNullOrEmpty(searchFilter.getCpu_brand())) {
			query.setParameter("cpu_brand", "%"+searchFilter.getCpu_brand()+"%");
		}
		return query.getResultList();
	}

	@Override
	public Long countCpus(Cpu searchFilter) {
		// TODO Auto-generated method stub
		StringBuilder sqlQuery = new StringBuilder("SELECT * FROM cpu cpu ");
		if(!StringUtility.isStringNullOrEmpty(searchFilter.getCpu_brand())) {
			sqlQuery.append(" WHERE cpu.cpu_brand LIKE :cpu_brand ");
		}
		
		Query query = entityManager.createNativeQuery(sqlQuery.toString(), Cpu.class);
		
		if(!StringUtility.isStringNullOrEmpty(searchFilter.getCpu_brand())) {
			query.setParameter("cpu_brand", "%"+searchFilter.getCpu_brand()+"%");
		}
		query.unwrap(SQLQuery.class).addScalar("customerCount", LongType.INSTANCE);
		
		return (Long) query.getSingleResult();
	}
	
	
}
