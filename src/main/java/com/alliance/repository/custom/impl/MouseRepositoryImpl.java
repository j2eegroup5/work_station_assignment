package com.alliance.repository.custom.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.SQLQuery;
import org.hibernate.type.LongType;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.alliance.entity.Mouse;
import com.alliance.repository.custom.MouseRepositoryCustom;
import com.alliance.utility.StringUtility;

@Repository("mouseRepositoryCustom")
@Transactional(readOnly=true)
public class MouseRepositoryImpl extends GenericRepositoryImpl implements MouseRepositoryCustom{
	
	@PersistenceContext
	EntityManager entityManager;

	@Override
	public List<Mouse> listMouses(Mouse searchFilter) {
		// TODO Auto-generated method stub
		StringBuilder sqlQuery = new StringBuilder("SELECT * FROM mouse mouse ");
		if(!StringUtility.isStringNullOrEmpty(searchFilter.getMouse_brand())) {
			sqlQuery.append(" WHERE mouse.mouse_brand LIKE :mouse_brand ");
		}
		
		Query query = entityManager.createNativeQuery(sqlQuery.toString(), Mouse.class);
		
		if(!StringUtility.isStringNullOrEmpty(searchFilter.getMouse_brand())) {
			query.setParameter("mouse_brand", "%"+searchFilter.getMouse_brand()+"%");
		}
		return query.getResultList();
	}

	@Override
	public Long countMouses(Mouse searchFilter) {
		// TODO Auto-generated method stub
		StringBuilder sqlQuery = new StringBuilder("SELECT * FROM mouse mouse ");
		if(!StringUtility.isStringNullOrEmpty(searchFilter.getMouse_brand())) {
			sqlQuery.append(" WHERE mouse.mouse_brand LIKE :mouse_brand ");
		}
		
		Query query = entityManager.createNativeQuery(sqlQuery.toString(), Mouse.class);
		
		if(!StringUtility.isStringNullOrEmpty(searchFilter.getMouse_brand())) {
			query.setParameter("mouse_brand", "%"+searchFilter.getMouse_brand()+"%");
		}
		query.unwrap(SQLQuery.class).addScalar("customerCount", LongType.INSTANCE);
		
		return (Long) query.getSingleResult();
	}
	
	
}
