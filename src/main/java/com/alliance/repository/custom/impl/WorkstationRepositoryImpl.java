package com.alliance.repository.custom.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.SQLQuery;
import org.hibernate.type.LongType;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.alliance.entity.Workstation;
import com.alliance.repository.custom.WorkstationRepositoryCustom;
import com.alliance.utility.StringUtility;

@Repository("workstationRepositoryCustom")
@Transactional(readOnly=true)
public class WorkstationRepositoryImpl extends GenericRepositoryImpl implements WorkstationRepositoryCustom{
	
	@PersistenceContext
	EntityManager entityManager;

	@Override
	public List<Workstation> listWorkstations(Workstation searchFilter) {
		// TODO Auto-generated method stub
		StringBuilder sqlQuery = new StringBuilder("SELECT * FROM workstation workstation ");
		
		
		Query query = entityManager.createNativeQuery(sqlQuery.toString(), Workstation.class);
		
		
		return query.getResultList();
	}

	@Override
	public Long countWorkstations(Workstation searchFilter) {
		// TODO Auto-generated method stub
		StringBuilder sqlQuery = new StringBuilder("SELECT * FROM workstation workstation ");
		
		
		Query query = entityManager.createNativeQuery(sqlQuery.toString(), Workstation.class);
		
		
		query.unwrap(SQLQuery.class).addScalar("customerCount", LongType.INSTANCE);
		
		return (Long) query.getSingleResult();
	}
	
	
}
