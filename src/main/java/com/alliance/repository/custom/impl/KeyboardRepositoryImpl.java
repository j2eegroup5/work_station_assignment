package com.alliance.repository.custom.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.SQLQuery;
import org.hibernate.type.LongType;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.alliance.entity.Keyboard;
import com.alliance.repository.custom.KeyboardRepositoryCustom;
import com.alliance.utility.StringUtility;

@Repository("keyboardRepositoryCustom")
@Transactional(readOnly=true)
public class KeyboardRepositoryImpl extends GenericRepositoryImpl implements KeyboardRepositoryCustom{
	
	@PersistenceContext
	EntityManager entityManager;

	@Override
	public List<Keyboard> listKeyboards(Keyboard searchFilter) {
		// TODO Auto-generated method stub
		StringBuilder sqlQuery = new StringBuilder("SELECT * FROM keyboard keyboard ");
		if(!StringUtility.isStringNullOrEmpty(searchFilter.getKeyboard_brand())) {
			sqlQuery.append(" WHERE screen.keyboard_brand LIKE :keyboard_brand ");
		}
		
		Query query = entityManager.createNativeQuery(sqlQuery.toString(), Keyboard.class);
		
		if(!StringUtility.isStringNullOrEmpty(searchFilter.getKeyboard_brand())) {
			query.setParameter("keyboard_brand", "%"+searchFilter.getKeyboard_brand()+"%");
		}
		return query.getResultList();
	}

	@Override
	public Long countKeyboards(Keyboard searchFilter) {
		// TODO Auto-generated method stub
		StringBuilder sqlQuery = new StringBuilder("SELECT * FROM keyboard keyboard ");
		if(!StringUtility.isStringNullOrEmpty(searchFilter.getKeyboard_brand())) {
			sqlQuery.append(" WHERE screen.keyboard_brand LIKE :keyboard_brand ");
		}
		
		Query query = entityManager.createNativeQuery(sqlQuery.toString(), Keyboard.class);
		
		if(!StringUtility.isStringNullOrEmpty(searchFilter.getKeyboard_brand())) {
			query.setParameter("keyboard_brand", "%"+searchFilter.getKeyboard_brand()+"%");
		}
		query.unwrap(SQLQuery.class).addScalar("customerCount", LongType.INSTANCE);
		
		return (Long) query.getSingleResult();
	}
	
	
}
