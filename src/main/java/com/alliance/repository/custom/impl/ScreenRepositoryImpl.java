package com.alliance.repository.custom.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.SQLQuery;
import org.hibernate.type.LongType;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.alliance.entity.Screen;
import com.alliance.repository.custom.ScreenRepositoryCustom;
import com.alliance.utility.StringUtility;

@Repository("screenRepositoryCustom")
@Transactional(readOnly=true)
public class ScreenRepositoryImpl extends GenericRepositoryImpl implements ScreenRepositoryCustom{
	
	@PersistenceContext
	EntityManager entityManager;

	@Override
	public List<Screen> listScreens(Screen searchFilter) {
		// TODO Auto-generated method stub
		StringBuilder sqlQuery = new StringBuilder("SELECT * FROM screen screen ");
		if(!StringUtility.isStringNullOrEmpty(searchFilter.getScreen_brand())) {
			sqlQuery.append(" WHERE screen.screen_brand LIKE :screen_brand ");
		}
		
		Query query = entityManager.createNativeQuery(sqlQuery.toString(), Screen.class);
		
		if(!StringUtility.isStringNullOrEmpty(searchFilter.getScreen_brand())) {
			query.setParameter("screen_brand", "%"+searchFilter.getScreen_brand()+"%");
		}
		return query.getResultList();
	}

	@Override
	public Long countScreens(Screen searchFilter) {
		// TODO Auto-generated method stub
		StringBuilder sqlQuery = new StringBuilder("SELECT * FROM screen screen ");
		if(!StringUtility.isStringNullOrEmpty(searchFilter.getScreen_brand())) {
			sqlQuery.append(" WHERE screen.screen_brand LIKE :screen_brand ");
		}
		
		Query query = entityManager.createNativeQuery(sqlQuery.toString(), Screen.class);
		
		if(!StringUtility.isStringNullOrEmpty(searchFilter.getScreen_brand())) {
			query.setParameter("screen_brand", "%"+searchFilter.getScreen_brand()+"%");
		}
		query.unwrap(SQLQuery.class).addScalar("customerCount", LongType.INSTANCE);
		
		return (Long) query.getSingleResult();
	}
	
	
}
