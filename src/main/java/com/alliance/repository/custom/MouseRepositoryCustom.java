package com.alliance.repository.custom;

import java.util.List;

import com.alliance.entity.Mouse;

public interface MouseRepositoryCustom extends GenericRepositoryCustom {
	
	public List<Mouse> listMouses(Mouse searchFilter);
	
	public Long countMouses(Mouse searchFilter);
}
