package com.alliance.repository.custom;

import java.util.List;

import com.alliance.entity.Keyboard;

public interface KeyboardRepositoryCustom extends GenericRepositoryCustom {
	
	public List<Keyboard> listKeyboards(Keyboard searchFilter);
	
	public Long countKeyboards(Keyboard searchFilter);
}
