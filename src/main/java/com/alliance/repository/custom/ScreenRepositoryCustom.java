package com.alliance.repository.custom;

import java.util.List;

import com.alliance.entity.Screen;

public interface ScreenRepositoryCustom extends GenericRepositoryCustom {
	
	public List<Screen> listScreens(Screen searchFilter);
	
	public Long countScreens(Screen searchFilter);
}
