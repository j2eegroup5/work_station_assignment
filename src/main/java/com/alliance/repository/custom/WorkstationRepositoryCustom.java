package com.alliance.repository.custom;

import java.util.List;

import com.alliance.entity.Workstation;

public interface WorkstationRepositoryCustom extends GenericRepositoryCustom {
	
	public List<Workstation> listWorkstations(Workstation searchFilter);
	
	public Long countWorkstations(Workstation searchFilter);
}
