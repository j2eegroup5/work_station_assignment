package com.alliance.repository.custom;

import java.util.List;

import com.alliance.entity.Cpu;

public interface CpuRepositoryCustom extends GenericRepositoryCustom {
	
	public List<Cpu> listCpus(Cpu searchFilter);
	
	public Long countCpus(Cpu searchFilter);
}
