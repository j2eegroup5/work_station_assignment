package com.alliance.repository;

import com.alliance.entity.Workstation;
import com.alliance.repository.custom.WorkstationRepositoryCustom;
import com.alliance.repository.custom.GenericRepositoryCustom;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("workstationRepository")
public interface WorkstationRepository extends JpaRepository<Workstation, Integer>, WorkstationRepositoryCustom, GenericRepositoryCustom {
	public Workstation findOneById(Integer workstationId);
}
