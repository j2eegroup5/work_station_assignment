package com.alliance.repository;

import org.springframework.data.repository.query.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.alliance.entity.Admin;

@Repository
public interface AdminRepository extends JpaRepository<Admin, String> {
	
	Admin findOneByAdminId(int adminId);

	@Query("select a from #{#entityName} a where a.username = ?1 and a.password = ?2")
	Admin getAdminByUsernameAndPassword(String username, String password);
	
	@Modifying
	@Query("update #{#entityName} s set s.fname = ?2, s.lname = ?3, s.username = ?4, s.password = ?5 where s.adminId = ?1")
	Admin updateAdmin(int adminId, String fname, String lname, String username, String password);
	
	@Modifying
	@Query(value = "INSERT INTO admin (admin_fname,admin_lname,admin_username,admin_password) VALUES (:fname,:lname,:username,:password)", nativeQuery = true)
	void addAdmin(@Param("fname") String firstName, @Param("lname") String lastName, @Param("username") String username, 
			      @Param("password") String password);
	
	@Query("delete from #{#entityName} a where a.adminId = ?1")
	void deleteAdmin(int adminId);

}
