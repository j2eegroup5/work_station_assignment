package com.alliance.repository;

import com.alliance.entity.Cpu;
import com.alliance.repository.custom.CpuRepositoryCustom;
import com.alliance.repository.custom.GenericRepositoryCustom;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("cpuRepository")
public interface CpuRepository extends JpaRepository<Cpu, Integer>, CpuRepositoryCustom, GenericRepositoryCustom {
	public Cpu findOneById(Integer cpuId);
}
