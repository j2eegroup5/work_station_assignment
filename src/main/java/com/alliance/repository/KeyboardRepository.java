package com.alliance.repository;

import com.alliance.entity.Keyboard;
import com.alliance.repository.custom.KeyboardRepositoryCustom;
import com.alliance.repository.custom.GenericRepositoryCustom;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("keyboardRepository")
public interface KeyboardRepository extends JpaRepository<Keyboard, Integer>, KeyboardRepositoryCustom, GenericRepositoryCustom {
	public Keyboard findOneById(Integer keyboardId);
}
