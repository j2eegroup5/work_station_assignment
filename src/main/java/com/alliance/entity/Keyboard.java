package com.alliance.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.alliance.model.SearchFilter;

@Entity
@Table(name="keyboard", schema="twas")
public class Keyboard extends SearchFilter implements Serializable{
	private int id;
	private String keyboard_brand;
	
	@Id
	@Column(name = "id", nullable=false)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@Basic
	@Column(name="keyboard_brand")
	public String getKeyboard_brand() {
		return keyboard_brand;
	}
	public void setKeyboard_brand(String keyboard_brand) {
		this.keyboard_brand = keyboard_brand;
	}
	
	@Override
	public boolean equals(Object o) {
		if(this==o)
			return true;
		if(o==null||getClass()!=o.getClass())
			return false;
		Keyboard keyboard = (Keyboard) o;
		return id == keyboard.id && Objects.equals(keyboard_brand, keyboard.keyboard_brand);
	}
	
	public void setInfo(Keyboard keyboard) {
		this.keyboard_brand = keyboard.getKeyboard_brand();
	}
	
	@Override
	public String toString() {
		return "Keyboard [id="+id+", keyboard_brand="+keyboard_brand+", pageSize="+getPageSize()+", page="+getPage()+ "]";
	}
	
}
