package com.alliance.entity;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.alliance.model.SearchFilter;

@Entity
@Table(name = "trainee", schema = "tsc", catalog = "")
public class Trainee extends SearchFilter implements Serializable{
	
	@Id
	@Column(name = "TraineeID", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Basic
	@Column(name = "FirstName", nullable = false, length = 64)
	private String FirstName;
	@Basic
	@Column(name = "LastName", nullable = false, length = 64)
	private String LastName;
	@Basic
	@Column(name = "Address", nullable = false, length = 255)
	private String Address;
	@Basic
	@Column(name = "City", nullable = false, length = 255)
	private String City;

	//get/set
	public int getTraineeID() 
	{
		return id;
	}

	public void setTraineeID(int traineeID) 
	{
		id = traineeID;
	}

	public String getFirstName() 
	{
		return FirstName;
	}

	public void setFirstName(String firstName) 
	{
		FirstName = firstName;
	}

	public String getLastName() 
	{
		return LastName;
	}

	public void setLastName(String lastName) 
	{
		LastName = lastName;
	}

	public String getAddress() 
	{
		return Address;
	}

	public void setAddress(String address) 
	{
		Address = address;
	}

	public String getCity() 
	{
		return City;
	}

	public void setCity(String city) 
	{
		City = city;
	}

	//security. DONT MIND THIS YET.
	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Trainee trainee = (Trainee) o;
		return id == trainee.id && Objects.equals(FirstName , trainee.FirstName ) && Objects.equals(LastName, trainee.LastName) && Objects.equals(Address, trainee.Address) && Objects.equals(City, trainee.City);
	}
	
	//hash only
	@Override
	public int hashCode() {

		return Objects.hash(id, FirstName, LastName, Address, City);
	}
	
	//set only
	public void setInfo(Trainee trainee) {
		this.FirstName = trainee.getFirstName();
		this.LastName = trainee.getLastName();
		this.Address = trainee.getAddress();
		this.City = trainee.getCity();
	}
	
	//tostring only
	@Override
	public String toString() {
		return "Customer [id=" + id + ", First Name=" + FirstName + ", Last Name=" + LastName + ", Address=" + Address + ", City=" + City + "]";
	}
}
