package com.alliance.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.alliance.model.SearchFilter;

@Entity
@Table(name="mouse", schema="twas")
public class Mouse extends SearchFilter implements Serializable{
	private int id;
	private String mouse_brand;
	
	@Id
	@Column(name = "id", nullable=false)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@Basic
	@Column(name="mouse_brand")
	public String getMouse_brand() {
		return mouse_brand;
	}
	public void setMouse_brand(String mouse_brand) {
		this.mouse_brand = mouse_brand;
	}
	
	@Override
	public boolean equals(Object o) {
		if(this==o)
			return true;
		if(o==null||getClass()!=o.getClass())
			return false;
		Mouse mouse = (Mouse) o;
		return id == mouse.id && Objects.equals(mouse_brand, mouse.mouse_brand);
	}
	
	public void setInfo(Mouse mouse) {
		this.mouse_brand = mouse.getMouse_brand();
	}
	
	@Override
	public String toString() {
		return "Mouse [id="+id+", mouse_brand="+mouse_brand+", pageSize="+getPageSize()+", page="+getPage()+ "]";
	}
	
}
