package com.alliance.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.alliance.model.SearchFilter;

@Entity
@Table(name="cpu", schema="twas")
public class Cpu extends SearchFilter implements Serializable{
	private int id;
	private String cpu_brand;
	
	@Id
	@Column(name = "id", nullable=false)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@Basic
	@Column(name="cpu_brand")
	public String getCpu_brand() {
		return cpu_brand;
	}
	public void setCpu_brand(String cpu_brand) {
		this.cpu_brand = cpu_brand;
	}
	
	@Override
	public boolean equals(Object o) {
		if(this==o)
			return true;
		if(o==null||getClass()!=o.getClass())
			return false;
		Cpu cpu = (Cpu) o;
		return id == cpu.id && Objects.equals(cpu_brand, cpu.cpu_brand);
	}
	
	public void setInfo(Cpu cpu) {
		this.cpu_brand = cpu.getCpu_brand();
	}
	
	@Override
	public String toString() {
		return "Cpu [id="+id+", cpu_brand="+cpu_brand+", pageSize="+getPageSize()+", page="+getPage()+ "]";
	}
	
}
