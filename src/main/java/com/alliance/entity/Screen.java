package com.alliance.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.alliance.model.SearchFilter;

@Entity
@Table(name="screen", schema="twas")
public class Screen extends SearchFilter implements Serializable{
	private int id;
	private String screen_brand;
	
	@Id
	@Column(name = "id", nullable=false)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@Basic
	@Column(name="screen_brand")
	public String getScreen_brand() {
		return screen_brand;
	}
	public void setScreen_brand(String screen_brand) {
		this.screen_brand = screen_brand;
	}
	
	@Override
	public boolean equals(Object o) {
		if(this==o)
			return true;
		if(o==null||getClass()!=o.getClass())
			return false;
		Screen screen = (Screen) o;
		return id == screen.id && Objects.equals(screen_brand, screen.screen_brand);
	}
	
	public void setInfo(Screen screen) {
		this.screen_brand = screen.getScreen_brand();
	}
	
	@Override
	public String toString() {
		return "Screen [id="+id+", screen_brand="+screen_brand+", pageSize="+getPageSize()+", page="+getPage()+ "]";
	}
	
}
