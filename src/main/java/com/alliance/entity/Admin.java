package com.alliance.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "admin", schema = "workstationassignment", catalog = "")
public class Admin {

	public Admin() {}
	
	int adminId;
	String fname;
	String lname;
	String username;
	String password;
	
	@Id @GeneratedValue
	@Column( name = "admin_id", nullable = false, length = 11)
	public int getAdminId() {
		return adminId;
	}
	public void setAdminId(int adminId) {
		this.adminId = adminId;
	}
	
	@Column( name = "admin_fname", nullable = false, length = 30)
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	
	@Column( name = "admin_lname", nullable = false, length = 30)
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	
	@Column( name = "admin_username", nullable = false, length = 30)
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	@Column( name = "admin_password", nullable = false, length = 100)
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
