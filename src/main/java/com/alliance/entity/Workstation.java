package com.alliance.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.tomcat.jni.Address;

import com.alliance.model.SearchFilter;

@Entity
@Table(name = "workstation")
public class Workstation extends SearchFilter implements Serializable {
	private int id;
	private int cpuID;
	private int mouseID;
	private int screenID;
	private int keyboardID;


	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id", nullable = true)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="traineeID",referencedColumnName="TraineeID")
	private Trainee trainee;
	public Trainee getTrainee() {
		return trainee;
	}

	public void setTrainee(Trainee trainee) {
		this.trainee = trainee;
	}

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="cpu_id",referencedColumnName="id")
	private Cpu cpu;
	public Cpu getCpu() {
		return cpu;
	}

	public void setCpu(Cpu cpu) {
		this.cpu = cpu;
	}

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="mouse_id",referencedColumnName="id")
	private Mouse mouse;
	public Mouse getMouse() {
		return mouse;
	}

	public void setMouse(Mouse mouse) {
		this.mouse = mouse;
	}

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="screen_id",referencedColumnName="id")
	private Screen screen;
	public Screen getScreen() {
		return screen;
	}

	public void setScreen(Screen screen) {
		this.screen = screen;
	}

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="keyboard_id",referencedColumnName="id")
	private Keyboard keyboard;
	public Keyboard getKeyboard() {
		return keyboard;
	}

	public void setKeyboard(Keyboard keyboard) {
		this.keyboard = keyboard;
	}
	
	public void setInfo() {
		this.keyboard = keyboard;
		this.trainee = trainee;
		this.cpu = cpu;
		this.mouse = mouse;
		this.screen = screen;
		
	}
	
//	public void setInfo(Workstation workstation) {
//		this.trainee.etTraineeID() = workstation.getTraineeID();
//		this.cpuID = workstation.getCpuID();
//		this.keyboardID = workstation.getKeyboardID();
//		this.mouseID = workstation.getMouseID();
//		this.screenID = workstation.getScreenID();
//	}
	
	@Override
	public boolean equals(Object o) {
		if(this==o)
			return true;
		if(o==null||getClass()!=o.getClass())
			return false;
		Workstation workstation = (Workstation) o;
		return id == workstation.id && Objects.equals(screenID, workstation.screenID)&& Objects.equals(cpuID, workstation.cpuID)&& Objects.equals(keyboardID, workstation.keyboardID)&& Objects.equals(mouseID, workstation.mouseID);
	}
	
	@Override
	public String toString() {
		return "Workstation [id="+id+", screenID="+screenID+", mouseID="+mouseID+", keyboardID="+keyboardID+", cpuID="+cpuID+", pageSize="+getPageSize()+", page="+getPage()+ "]";
	}
	


}