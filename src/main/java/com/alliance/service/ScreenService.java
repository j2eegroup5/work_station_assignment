package com.alliance.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alliance.constants.StringConstants;
import com.alliance.entity.Screen;
import com.alliance.model.ListResult;
import com.alliance.model.ScreenSearchFilter;
import com.alliance.repository.ScreenRepository;

@Service("screenService")
public class ScreenService {
	
	@Autowired
	private ScreenRepository screenRepository;
	
	public List<Screen> getScreenList(Screen searchFilter){
		List<Screen> screenList = screenRepository.listScreens(searchFilter);
		return screenList;
	}
	
	public ListResult getScreenListResult(ScreenSearchFilter searchFilter) {
		List<Screen> screenGeneric = (List<Screen>) screenRepository.searchTable(Screen.class, "screen", searchFilter, StringConstants.screenSearchParameters, StringConstants.screenSearchParametersMap);
		Long screenListCount = screenRepository.getResultCount("screen", searchFilter, StringConstants.screenSearchParameters, StringConstants.screenSearchParametersMap);
		for(Screen screen : screenGeneric)
			System.out.println("Screen - "+screen.toString());
		return new ListResult(screenGeneric, screenListCount);
	}
	
	public boolean insertScreen(Screen screen) {
		Screen insertedScreen = screenRepository.saveAndFlush(screen);
		if(insertedScreen!=null) {
			return true;
		}
		return false;
	}
	
	public Screen getScreenById(Integer screenId) {
		return screenRepository.findOneById(screenId);
	}
	
	public Screen updateScreenInfo(Screen screen) {
		Screen fromDBScreen = screenRepository.findOneById(screen.getId());
		fromDBScreen.setInfo(screen);
		return screenRepository.saveAndFlush(fromDBScreen);
	}
}
