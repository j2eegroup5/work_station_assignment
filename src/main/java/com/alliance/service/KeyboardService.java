package com.alliance.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alliance.constants.StringConstants;
import com.alliance.entity.Keyboard;
import com.alliance.model.ListResult;
import com.alliance.model.KeyboardSearchFilter;
import com.alliance.repository.KeyboardRepository;

@Service("keyboardService")
public class KeyboardService {
	
	@Autowired
	private KeyboardRepository keyboardRepository;
	
	public List<Keyboard> getKeyboardList(Keyboard searchFilter){
		List<Keyboard> keyboardList = keyboardRepository.listKeyboards(searchFilter);
		return keyboardList;
	}
	
	public ListResult getKeyboardListResult(KeyboardSearchFilter searchFilter) {
		List<Keyboard> keyboardGeneric = (List<Keyboard>) keyboardRepository.searchTable(Keyboard.class, "keyboard", searchFilter, StringConstants.keyboardSearchParameters, StringConstants.keyboardSearchParametersMap);
		Long keyboardListCount = keyboardRepository.getResultCount("keyboard", searchFilter, StringConstants.keyboardSearchParameters, StringConstants.keyboardSearchParametersMap);
		for(Keyboard keyboard : keyboardGeneric)
			System.out.println("Keyboard - "+keyboard.toString());
		return new ListResult(keyboardGeneric, keyboardListCount);
	}
	
	public boolean insertKeyboard(Keyboard keyboard) {
		Keyboard insertedKeyboard = keyboardRepository.saveAndFlush(keyboard);
		if(insertedKeyboard!=null) {
			return true;
		}
		return false;
	}
	
	public Keyboard getKeyboardById(Integer keyboardId) {
		return keyboardRepository.findOneById(keyboardId);
	}
	
	public Keyboard updateKeyboardInfo(Keyboard keyboard) {
		Keyboard fromDBKeyboard = keyboardRepository.findOneById(keyboard.getId());
		fromDBKeyboard.setInfo(keyboard);
		return keyboardRepository.saveAndFlush(fromDBKeyboard);
	}
}
