package com.alliance.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alliance.constants.StringConstants;
import com.alliance.entity.Mouse;
import com.alliance.model.ListResult;
import com.alliance.model.MouseSearchFilter;
import com.alliance.repository.MouseRepository;

@Service("mouseService")
public class MouseService {
	
	@Autowired
	private MouseRepository mouseRepository;
	
	public List<Mouse> getMouseList(Mouse searchFilter){
		List<Mouse> mouseList = mouseRepository.listMouses(searchFilter);
		return mouseList;
	}
	
	public ListResult getMouseListResult(MouseSearchFilter searchFilter) {
		List<Mouse> mouseGeneric = (List<Mouse>) mouseRepository.searchTable(Mouse.class, "mouse", searchFilter, StringConstants.mouseSearchParameters, StringConstants.mouseSearchParametersMap);
		Long mouseListCount = mouseRepository.getResultCount("mouse", searchFilter, StringConstants.mouseSearchParameters, StringConstants.mouseSearchParametersMap);
		for(Mouse mouse : mouseGeneric)
			System.out.println("Mouse - "+mouse.toString());
		return new ListResult(mouseGeneric, mouseListCount);
	}
	
	public boolean insertMouse(Mouse mouse) {
		Mouse insertedMouse = mouseRepository.saveAndFlush(mouse);
		if(insertedMouse!=null) {
			return true;
		}
		return false;
	}
	
	public Mouse getMouseById(Integer mouseId) {
		return mouseRepository.findOneById(mouseId);
	}
	
	public Mouse updateMouseInfo(Mouse mouse) {
		Mouse fromDBMouse = mouseRepository.findOneById(mouse.getId());
		fromDBMouse.setInfo(mouse);
		return mouseRepository.saveAndFlush(fromDBMouse);
	}
}
