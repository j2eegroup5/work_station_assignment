package com.alliance.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alliance.constants.StringConstants;
import com.alliance.entity.Cpu;
import com.alliance.model.ListResult;
import com.alliance.model.CpuSearchFilter;
import com.alliance.repository.CpuRepository;

@Service("cpuService")
public class CpuService {
	
	@Autowired
	private CpuRepository cpuRepository;
	
	public List<Cpu> getCpuList(Cpu searchFilter){
		List<Cpu> cpuList = cpuRepository.listCpus(searchFilter);
		return cpuList;
	}
	
	public ListResult getCpuListResult(CpuSearchFilter searchFilter) {
		List<Cpu> cpuGeneric = (List<Cpu>) cpuRepository.searchTable(Cpu.class, "cpu", searchFilter, StringConstants.cpuSearchParameters, StringConstants.cpuSearchParametersMap);
		Long cpuListCount = cpuRepository.getResultCount("cpu", searchFilter, StringConstants.cpuSearchParameters, StringConstants.cpuSearchParametersMap);
		for(Cpu cpu : cpuGeneric)
			System.out.println("Cpu - "+cpu.toString());
		return new ListResult(cpuGeneric, cpuListCount);
	}
	
	public boolean insertCpu(Cpu cpu) {
		Cpu insertedCpu = cpuRepository.saveAndFlush(cpu);
		if(insertedCpu!=null) {
			return true;
		}
		return false;
	}
	
	public Cpu getCpuById(Integer cpuId) {
		return cpuRepository.findOneById(cpuId);
	}
	
	public Cpu updateCpuInfo(Cpu cpu) {
		Cpu fromDBCpu = cpuRepository.findOneById(cpu.getId());
		fromDBCpu.setInfo(cpu);
		return cpuRepository.saveAndFlush(fromDBCpu);
	}
}
