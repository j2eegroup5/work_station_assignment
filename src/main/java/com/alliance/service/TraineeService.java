package com.alliance.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alliance.constants.StringConstants;
import com.alliance.entity.Trainee;
import com.alliance.model.ListResult;
import com.alliance.repository.TraineeRepository;

@Service()
public class TraineeService {

	//autowired so you no longer need to instantiate
	@Autowired
	private TraineeRepository traineeRepository;

	//index or find all. These methods are to be called in the controller.
	public List<Trainee> findAllTrainees() {
		return traineeRepository.findAll();
	}

	//save
	public boolean saveTrainees(Trainee trainee) 
	{
		try
		{
	        traineeRepository.saveAndFlush(trainee);
	        return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
    }
	//delete
	public boolean deleteTrainees(Integer traineeID) 
	{
		try
		{
			Trainee traineeInstance = traineeRepository.findOneById(traineeID);
	        traineeRepository.delete(traineeInstance);
	        return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
    }
	//update not yet working.
	public boolean updateTrainees(Trainee trainee) 
	{
		try
		{
			Trainee traineeInstance = traineeRepository.findOneById(trainee.getTraineeID());
	        traineeInstance = trainee;
			traineeRepository.saveAndFlush(traineeInstance);
	        return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
    }
	/*public boolean updateTrainees(Trainee trainee) 
	{
		try
		{
			Trainee updateTrainee = traineeRepository.findOneById(trainee.getTraineeID());
	        
			updateTrainee = trainee;
			traineeRepository.save(updateTrainee);
			
	        return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
    }*/
	/*public ListResult getCustomerListResult(CustomerSearchFilter searchFilter) {
		// List<Customer> customerList = customerRepository.listCustomers(searchFilter);
		List<Customer> customerGeneric = (List<Customer>) customerRepository.searchTable(Customer.class, "customer", searchFilter, StringConstants.customerSearchParameters, StringConstants.customerSearchParametersMap);
		Long customerListCount = customerRepository.getResultCount("customer", searchFilter, StringConstants.customerSearchParameters, StringConstants.customerSearchParametersMap);
		for (Customer customer : customerGeneric)
			System.out.println("CUSTOMER - " + customer.toString());
		return new ListResult(customerGeneric, customerListCount);
	}

	public boolean insertCustomer(Customer customer) {
		Customer insertedCustomer = customerRepository.saveAndFlush(customer);
		if (insertedCustomer != null) {
			return true;
		}
		return false;
	}

	public Customer getCustomerById(Integer customerId) {
		return customerRepository.findOneById(customerId);
	}
	public Customer updateCustomerInfo(Customer customer) {
		Customer fromDBCustomer = customerRepository.findOneById(customer.getId());
		fromDBCustomer.setInfo(customer);
		return customerRepository.saveAndFlush(fromDBCustomer);
	}*/

}
