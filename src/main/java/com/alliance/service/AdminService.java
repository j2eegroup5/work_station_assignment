package com.alliance.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alliance.entity.Admin;
import com.alliance.repository.AdminRepository;

@Transactional
@Service
public class AdminService {

	public AdminService() {}
	
	@Autowired
	public AdminRepository adminRepository;
	
	public Admin getAdminByUsernameAndPassword(String username, String password) {
		return adminRepository.getAdminByUsernameAndPassword(username, password);
	}
	
	public Admin findOneByAdminId(int adminId) {
		return adminRepository.findOneByAdminId(adminId);
	}
	
	public void deleteAdmin(int adminId) {
		adminRepository.deleteAdmin(adminId);
	}
	
	public Admin updateAdmin(int adminId, String fname, String lname, String username, String password) {
		return adminRepository.updateAdmin(adminId, fname, lname, username, password);
		
	}
	
	public void addAdmin(Admin admin) {
		String firstName = admin.getFname();
		String lastName = admin.getLname();
		String username = admin.getUsername();
		String password = admin.getPassword();
		
		adminRepository.addAdmin(firstName, lastName, username, password);
	}
}
