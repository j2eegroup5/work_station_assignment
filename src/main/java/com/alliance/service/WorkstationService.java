package com.alliance.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alliance.constants.StringConstants;
import com.alliance.entity.Workstation;
import com.alliance.model.ListResult;
import com.alliance.model.WorkstationSearchFilter;
import com.alliance.repository.WorkstationRepository;

@Service("workstationService")
public class WorkstationService {
	
	@Autowired
	private WorkstationRepository workstationRepository;
	
	public List<Workstation> getWorkstationList(Workstation searchFilter){
		List<Workstation> workstationList = workstationRepository.listWorkstations(searchFilter);
		return workstationList;
	}
	
	public ListResult getWorkstationListResult(WorkstationSearchFilter searchFilter) {
		List<Workstation> workstationGeneric = (List<Workstation>) workstationRepository.searchTable(Workstation.class, "workstation", searchFilter, StringConstants.workstationSearchParameters, StringConstants.workstationSearchParametersMap);
		Long workstationListCount = workstationRepository.getResultCount("workstation", searchFilter, StringConstants.workstationSearchParameters, StringConstants.workstationSearchParametersMap);
		for(Workstation workstation : workstationGeneric)
			System.out.println("Workstation - "+workstation.toString());
		return new ListResult(workstationGeneric, workstationListCount);
	}
	
	public boolean insertWorkstation(Workstation workstation) {
		Workstation insertedWorkstation = workstationRepository.saveAndFlush(workstation);
		if(insertedWorkstation!=null) {
			return true;
		}
		return false;
	}
	
	public Workstation getWorkstationById(Integer workstationId) {
		return workstationRepository.findOneById(workstationId);
	}
	
	public Workstation updateWorkstationInfo(Workstation workstation) {
		Workstation fromDBWorkstation = workstationRepository.findOneById(workstation.getId());
		//fromDBWorkstation.setInfo(workstation);
		return workstationRepository.saveAndFlush(fromDBWorkstation);
	}
}
