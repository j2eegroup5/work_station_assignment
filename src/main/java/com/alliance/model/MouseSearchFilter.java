package com.alliance.model;

public class MouseSearchFilter extends SearchFilter{
	private String id;
	private String mouse_brand;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMouse_brand() {
		return mouse_brand;
	}
	public void setMouse_brand(String mouse_brand) {
		this.mouse_brand = mouse_brand;
	}
	
	@Override
	public String toString() {
		return "MouseSearchFilter [id="+ id + ", mouse_brand="+mouse_brand+"]";
	}
}
