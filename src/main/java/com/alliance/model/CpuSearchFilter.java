package com.alliance.model;

public class CpuSearchFilter extends SearchFilter{
	private String id;
	private String cpu_brand;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCpu_brand() {
		return cpu_brand;
	}
	public void setCpu_brand(String cpu_brand) {
		this.cpu_brand = cpu_brand;
	}
	
	@Override
	public String toString() {
		return "CpuSearchFilter [id="+ id + ", cpu_brand="+cpu_brand+"]";
	}
}
