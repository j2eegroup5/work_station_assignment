package com.alliance.model;

public class ScreenSearchFilter extends SearchFilter{
	private String id;
	private String screen_brand;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getScreen_brand() {
		return screen_brand;
	}
	public void setScreen_brand(String screen_brand) {
		this.screen_brand = screen_brand;
	}
	
	@Override
	public String toString() {
		return "ScreenSearchFilter [id="+ id + ", screen_brand="+screen_brand+"]";
	}
}
