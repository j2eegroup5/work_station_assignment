package com.alliance.model;

public class KeyboardSearchFilter extends SearchFilter{
	private String id;
	private String keyboard_brand;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getKeyboard_brand() {
		return keyboard_brand;
	}
	public void setKeyboard_brand(String keyboard_brand) {
		this.keyboard_brand = keyboard_brand;
	}
	
	@Override
	public String toString() {
		return "KeyboardSearchFilter [id="+ id + ", keyboard_brand="+keyboard_brand+"]";
	}
}
