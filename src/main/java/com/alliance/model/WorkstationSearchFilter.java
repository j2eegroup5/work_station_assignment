package com.alliance.model;

public class WorkstationSearchFilter extends SearchFilter{
	private String workstationID;
	private String traineeID;
	private String cpuID;
	private String mouseID;
	private String screenID;
	private String keyboardID;
	public String getWorkstationID() {
		return workstationID;
	}
	public void setWorkstationID(String workstationID) {
		this.workstationID = workstationID;
	}
	public String getTraineeID() {
		return traineeID;
	}
	public void setTraineeID(String traineeID) {
		this.traineeID = traineeID;
	}
	public String getCpuID() {
		return cpuID;
	}
	public void setCpuID(String cpuID) {
		this.cpuID = cpuID;
	}
	public String getMouseID() {
		return mouseID;
	}
	public void setMouseID(String mouseID) {
		this.mouseID = mouseID;
	}
	public String getScreenID() {
		return screenID;
	}
	public void setScreenID(String screenID) {
		this.screenID = screenID;
	}
	public String getKeyboardID() {
		return keyboardID;
	}
	public void setKeyboardID(String keyboardID) {
		this.keyboardID = keyboardID;
	}
	
	@Override
	public String toString() {
		return "Workstation [id="+workstationID+", screenID="+screenID+", mouseID="+mouseID+", keyboardID="+keyboardID+", cpuID="+cpuID+ "]";
	}
}
