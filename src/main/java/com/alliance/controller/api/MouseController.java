package com.alliance.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alliance.entity.Mouse;
import com.alliance.model.ListResult;
import com.alliance.model.MouseSearchFilter;
import com.alliance.service.MouseService;

@RestController(value="mouseApiController")
@RequestMapping(value="/api/mouse")
public class MouseController {

	@Autowired
	private MouseService mouseService;
	
	@RequestMapping(method = RequestMethod.GET)
	public ListResult getMouseListResult(MouseSearchFilter searchFilter) {
		System.out.println("This is search filter - " + searchFilter.toString());
		
		return mouseService.getMouseListResult(searchFilter);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/{id}")
	public Mouse getMouseById(@PathVariable(name="id") Integer mouseId, Mouse mouse) {
		return mouseService.getMouseById(mouseId);
	}
	
	
	@RequestMapping(method = RequestMethod.POST, value = "/{id}")
	public Mouse updateMouseInfo(@PathVariable(name="id") Integer mouseId, Mouse mouse) {
		System.out.println("Mouse Brand = "+mouse.getMouse_brand());
		return mouseService.updateMouseInfo(mouse);
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public boolean addMouse(Mouse mouse) {
		return mouseService.insertMouse(mouse);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/info")
	public Mouse getMouseInfo() {
		return new Mouse();
	}
}
