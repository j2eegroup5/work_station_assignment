package com.alliance.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alliance.entity.Admin;
import com.alliance.entity.Customer;
import com.alliance.repository.AdminRepository;
import com.alliance.service.AdminService;
import com.alliance.utility.DataPackager;

@RestController
@RequestMapping(value = "/api/admin")
public class AdminController {

	public AdminController() {}
	
	@Autowired
	public AdminService adminService;
	
	@Autowired
	public AdminRepository adminRepository;
	
	@RequestMapping( method=RequestMethod.POST, value="/getAdminByUserAndPass")
	public Object getAdminByUsernameAndPassword(Admin admin){
		DataPackager datapackager = new DataPackager();
		
		String username = admin.getUsername();
		String password = admin.getPassword();
		
		admin = adminService.getAdminByUsernameAndPassword(username, password);
		datapackager.setDataPackager(admin);
		
		return datapackager;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/findOneByAdminId/{adminId}")
	public Object findOneByAdminId(@PathVariable(name = "adminId") int adminId) {
		DataPackager datapackager = new DataPackager();
		Admin details = adminService.findOneByAdminId(adminId);
		
		details.getAdminId();
		details.getFname();
		details.getLname();

		datapackager.setDataPackager(details);
		
		return datapackager;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/deleteAdmin/{adminId}")
	public void deleteAdmin(@PathVariable(name = "adminId") int adminId) {

		adminService.deleteAdmin(adminId);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/updateAdmin")
	public Object updateAdmin(Admin admin) {
		DataPackager datapackager = new DataPackager();
		int id = admin.getAdminId();		
		Admin details = adminService.findOneByAdminId(id);
		
		details.setFname(admin.getFname());		
		details.setLname(admin.getLname());
		details.setUsername(admin.getUsername());
		details.setPassword(admin.getPassword());
		
		datapackager.setDataPackager(details);
		adminRepository.saveAndFlush(details);
				
		return datapackager;
	}
	
	@RequestMapping( method=RequestMethod.POST, value="/addAdmin")
	public void addAdmin(Admin admin) {
		
		admin.setFname(admin.getFname());		
		admin.setLname(admin.getLname());
		admin.setUsername(admin.getUsername());
		admin.setPassword(admin.getPassword());
		
		adminService.addAdmin(admin);
	}

}
