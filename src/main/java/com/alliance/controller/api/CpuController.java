package com.alliance.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alliance.entity.Cpu;
import com.alliance.model.ListResult;
import com.alliance.model.CpuSearchFilter;
import com.alliance.service.CpuService;

@RestController(value="cpuApiController")
@RequestMapping(value="/api/cpu")
public class CpuController {

	@Autowired
	private CpuService cpuService;
	
	@RequestMapping(method = RequestMethod.GET)
	public ListResult getCpuListResult(CpuSearchFilter searchFilter) {
		System.out.println("This is search filter - " + searchFilter.toString());
		
		return cpuService.getCpuListResult(searchFilter);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/{id}")
	public Cpu getCpuById(@PathVariable(name="id") Integer cpuId, Cpu cpu) {
		return cpuService.getCpuById(cpuId);
	}
	
	
	@RequestMapping(method = RequestMethod.POST, value = "/{id}")
	public Cpu updateCpuInfo(@PathVariable(name="id") Integer cpuId, Cpu cpu) {
		System.out.println("Cpu Brand = "+cpu.getCpu_brand());
		return cpuService.updateCpuInfo(cpu);
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public boolean addCpu(Cpu cpu) {
		return cpuService.insertCpu(cpu);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/info")
	public Cpu getCpuInfo() {
		return new Cpu();
	}
}
