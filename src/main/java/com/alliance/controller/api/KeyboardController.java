package com.alliance.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alliance.entity.Keyboard;
import com.alliance.model.ListResult;
import com.alliance.model.KeyboardSearchFilter;
import com.alliance.service.KeyboardService;

@RestController(value="keyboardApiController")
@RequestMapping(value="/api/keyboard")
public class KeyboardController {

	@Autowired
	private KeyboardService keyboardService;
	
	@RequestMapping(method = RequestMethod.GET)
	public ListResult getKeyboardListResult(KeyboardSearchFilter searchFilter) {
		System.out.println("This is search filter - " + searchFilter.toString());
		
		return keyboardService.getKeyboardListResult(searchFilter);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/{id}")
	public Keyboard getKeyboardById(@PathVariable(name="id") Integer keyboardId, Keyboard keyboard) {
		return keyboardService.getKeyboardById(keyboardId);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/{id}")
	public Keyboard updateKeyboardInfo(@PathVariable(name="id") Integer keyboardId, Keyboard keyboard) {
		System.out.println("Keyboard Brand = "+keyboard.getKeyboard_brand());
		return keyboardService.updateKeyboardInfo(keyboard);
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public boolean addKeyboard(Keyboard keyboard) {
		return keyboardService.insertKeyboard(keyboard);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/info")
	public Keyboard getKeyboardInfo() {
		return new Keyboard();
	}
}
