package com.alliance.controller.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alliance.entity.Trainee;
import com.alliance.service.TraineeService;

//Fix for Invalid CORS Request Error
@CrossOrigin

@RestController
@RequestMapping(value = "/api/trainee")
public class TraineeController {
    @Autowired
    TraineeService traineeService;
	
    //calling findall from service.
    @GetMapping
    public List<Trainee> list(){
        return traineeService.findAllTrainees();
    }
    
    //calling insert
    @PostMapping()
    public boolean insertTrainee (Trainee addTrainee)
    {
    	return traineeService.saveTrainees(addTrainee);
    }
    
    //calling delete.
    //delete mapping placeholder name should be the same with deleteTrainee parameter name.
    @DeleteMapping("/delete/{traineeID}")
    public boolean deleteTrainee (@PathVariable Integer traineeID)
    {
    	return traineeService.deleteTrainees(traineeID);
    }
    
    //calling update.
    @PostMapping("/update/{traineeID}")
    public boolean updateTrainees (@PathVariable Integer traineeID, Trainee trainee)
    {
    	return traineeService.updateTrainees(trainee);
    }
    
}