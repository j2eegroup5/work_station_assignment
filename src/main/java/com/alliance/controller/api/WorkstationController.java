package com.alliance.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alliance.entity.Workstation;
import com.alliance.model.ListResult;
import com.alliance.model.WorkstationSearchFilter;
import com.alliance.service.WorkstationService;

@RestController(value="workstationApiController")
@RequestMapping(value="/api/workstation")
public class WorkstationController {

	@Autowired
	private WorkstationService workstationService;
	
	@RequestMapping(method = RequestMethod.GET)
	public ListResult getWorkstationListResult(WorkstationSearchFilter searchFilter) {
		System.out.println("This is search filter - " + searchFilter.toString());
		
		return workstationService.getWorkstationListResult(searchFilter);
	}
	
//	@RequestMapping(method=RequestMethod.GET, value="/{id}")
//	public Workstation getWorkstationById(@PathVariable(name="id") Integer workstationId, Workstation workstation) {
//		return workstationService.getWorkstationById(workstationId);
//	}
//	
//	
//	@RequestMapping(method = RequestMethod.POST, value = "/{id}")
//	public Workstation updateWorkstationInfo(@PathVariable(name="id") Integer workstationId, Workstation workstation) {
//		System.out.println("Workstation Brand = "+workstation.getId());
//		return workstationService.updateWorkstationInfo(workstation);
//	}
	
	@RequestMapping(method = RequestMethod.POST)
	public boolean addWorkstation(Workstation workstation) {
		return workstationService.insertWorkstation(workstation);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/info")
	public Workstation getWorkstationInfo() {
		return new Workstation();
	}
}
