package com.alliance.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alliance.entity.Screen;
import com.alliance.model.ListResult;
import com.alliance.model.ScreenSearchFilter;
import com.alliance.service.ScreenService;

@RestController(value="screenApiController")
@RequestMapping(value="/api/screen")
public class ScreenController {

	@Autowired
	private ScreenService screenService;
	
	@RequestMapping(method = RequestMethod.GET)
	public ListResult getScreenListResult(ScreenSearchFilter searchFilter) {
		System.out.println("This is search filter - " + searchFilter.toString());
		
		return screenService.getScreenListResult(searchFilter);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/{id}")
	public Screen getScreenById(@PathVariable(name="id") Integer screenId, Screen screen) {
		return screenService.getScreenById(screenId);
	}
	
	
	@RequestMapping(method = RequestMethod.POST, value = "/{id}")
	public Screen updateScreenInfo(@PathVariable(name="id") Integer screenId, Screen screen) {
		System.out.println("Screen Brand = "+screen.getScreen_brand());
		return screenService.updateScreenInfo(screen);
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public boolean addScreen(Screen screen) {
		return screenService.insertScreen(screen);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/info")
	public Screen getScreenInfo() {
		return new Screen();
	}
}
