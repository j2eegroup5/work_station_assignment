<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page import = "java.io.*,java.util.*" %>

<!DOCTYPE html>
<html>
    <head>
	
    <title>Admin Dashboard</title>
     <!--    
    <c:url value="/js/bootstrap.js" var="bootstrapJs" />
    <c:url value="/js/main.js" var="mainJs" />
    <c:url value="https://code.jquery.com/jquery-3.3.1.js" var="jqueryJs" />
    <c:url value="/js/popper.min.js" var="popperJs" />
        
    <c:url value="/css/custom.css" var="customCss" />
    <c:url value="/css/bootstrap-grid.css" var="bootstrapGrid" />
	<c:url value="/css/bootstrap-reboot.css" var="bootstrapReboot" />
	<c:url value="/css/bootstrap.css" var="bootstrapMain" />
	<c:url value="https://use.fontawesome.com/releases/v5.6.3/css/all.css" var="fontawesome" />
	
	<link href="${customCss}" rel="stylesheet" />
	<link href="${bootstrapGrid}" rel="stylesheet" />
	<link href="${bootstrapReboot}" rel="stylesheet" />
	<link href="${bootstrapMain}" rel="stylesheet" />
	<link href="${fontawesome}" rel="stylesheet" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous"/>
	 -->
    </head>
    <body>
    <div class="container-fluid">
    <h1>ADMIN DETAILS</h1>   
    <p>ID: <span id="id"></span></p>
    <p>NAME: <span id="name"></span></p>
    <br>
    <button id="updateButton">UPDATE DETAILS</button>
    
    </div>
		<script type="text/javascript" src="${jqueryJs}"></script>
		<script type="text/javascript" src="${popperJs}"></script>
		<script type="text/javascript" src="${bootstrapJs}"></script>
		<script type="text/javascript" src="${mainJs}"></script>
		<script type="text/javascript">
		$(document).ready(function(){
		    var searchParams = new URLSearchParams(window.location.search);
		    var id = searchParams.get('adminId');
		    
		  	$("#updateButton").click(function(){
		  		
				window.location.replace("${pageContext.request.contextPath}/admin/updateAdmin?adminId=" + id);

		  	});
			$.ajax({
				type: "get",
				url: "http://localhost:8010/api/admin/findOneByAdminId/"+id,
				dataType: "json",
				data:{
					adminId: id
				},
				success: function(DataPackager){
					var data = DataPackager.dataPackager;
					var id = data.adminId;
					var name = data.lname + ", " + data.fname;
					
					$('#id').text(id);
					$('#name').text(name);
					
					
				}
			});
			
		});
		</script>
        
    </body>
</html>