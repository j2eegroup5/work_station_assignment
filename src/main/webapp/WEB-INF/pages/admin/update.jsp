<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page import = "java.io.*,java.util.*" %>

<!DOCTYPE html>
<html>
    <head>
	
    <title>ADMIN UPDATE</title>
        
	<c:url value="/js/bootstrap.js" var="bootstrapJs" />
    <c:url value="/js/main.js" var="mainJs" />
    <c:url value="https://code.jquery.com/jquery-3.3.1.js" var="jqueryJs" />
    <c:url value="/js/popper.min.js" var="popperJs" />

    </head>
    <body>
    
    <button name="back" id="back" value="back">BACK</button><br/><br/>
    
	<form method="GET">
		ADMIN ID: 		
		<input type="text" id=adminId readOnly name="adminId" style="border:none;outline:none" />
		<br /><br>
		 FirstName
		<input type="text" id="fname" name="fname" />
		<br />
		LastName
		<input type="text" id="lname" name="lname" />
		<br /> 
		Username
		<input type="text" id="username" name="username" />
		<br /> 
		Password
		<input type="text" id="password" name="password" />
		<br><br>
		
		<button name="update" id="update" value="update">SAVE CHANGES</button>
		
	</form>

		<script type="text/javascript" src="${jqueryJs}"></script>
		<script type="text/javascript" src="${popperJs}"></script>
		<script type="text/javascript" src="${bootstrapJs}"></script>
		<script type="text/javascript" src="${mainJs}"></script>
		<script type="text/javascript">
		$(document).ready(function(){		
			
		    var searchParams = new URLSearchParams(window.location.search);
		    var id = searchParams.get('adminId');
		    var status = searchParams.get('update');
		    
		    $("#back").click(function(){
				window.location.replace("${pageContext.request.contextPath}/admin/dashboard?adminId=" + id);
		    });
		    
		    $("#fname").focus();
		    
		    if(status != null){
				window.location.replace("${pageContext.request.contextPath}/admin/dashboard?adminId=" + id);
		    }
		    
		    $("#update").click(function(){
			    var id = searchParams.get('adminId');			    
		    	var fname = $("#fname").val();
		    	var lname = $("#lname").val();
		    	var username = $("#username").val();
				var password = $("#password").val();						
			
				/*var details = new Object();
				
				details.adminId = id;
				details.fname = fname;
				details.lname = lname;
				details.username = username;
				details.password = password;
				
				var jsonString = JSON.stringify(details);
				
				alert(jsonString);*/
					
				
				$.ajax({ 
				    url:"http://localhost:8010/api/admin/updateAdmin",    
				    type:"GET", 
				    contentType: "application/json; charset=utf-8",
				    dateType: "json",
				    data: {
				    	adminId: id,
				    	fname: fname,
				    	lname: lname,
				    	username: username,
				    	password: password
				    },
				    success: function(){
						
					}
				});
				
			});
		    
			$.ajax({
				type: "get",
				url: "http://localhost:8010/api/admin/findOneByAdminId/"+id,
				dataType: "json",
				data:{
					adminId: id
				},
				success: function(DataPackager){
					var data = DataPackager.dataPackager;
					var id = data.adminId;
					var fname = data.fname;
					var lname = data.lname;
					var username = data.username;
					var password = data.password;
					
					$('#adminId').val(id);
					$('#fname').val(fname);
					$('#lname').val(lname);
					$('#username').val(username);
					$('#password').val(password);

					
				}
			});
			
		});
		</script>
        
    </body>
</html>