`... other page imports`
    import TestRoutes from '../Test/routes';
    
    const PageRoutes = [
        ...TestRoutes,
        `... other page routes`
    ];
    
    export default PageRoutes;