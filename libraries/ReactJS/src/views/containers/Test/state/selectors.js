
//Checks if user input is a number
function checkIfNumber(state){
    let errors = {};
    
    if(!isNaN(state)){
        errors.testvalue = "Input value is not a number."    
    }
    
    return errors;
}

export{
    checkIfNumber
};