import * as types from "./types";

const defaultState = {
    reducerVariable: "I'm the default value",
    testForm: {
        form: [
            {
                type: "text",
                label: "Update State Value",
                placeholder: "Update State Value",
                minLength: 1,
                maxLength: 125,
                name: "testvalue", //name of state to be used on form
                default: '' //default form state value
            },
        ]
    }
};

export default function reducer (state=defaultState, action) {
    switch (action.type) {
        case types.UPDATE_STATE:
            return {...state, reducerVariable: action.payload};
        default: return state;
    }
};

// export { default as test } from "../views/containers/Test/state";
export { default as test } from "./reducers.js";