import reducer from "./reducers";
    
import * as testOperations from "./operations";
import * as testSelectors from "./selectors";
import { TestRoutes } from "../routes";

export {
    testOperations,
    testSelectors,    
};

export default {
    reducer,
    TestRoutes
};