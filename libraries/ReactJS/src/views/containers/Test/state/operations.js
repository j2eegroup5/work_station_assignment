//Import the action creators
import * as Actions from "./actions";

//Import the ApiService utility if you need to call an API
import ApiService from "../../../../utils/apiService";
import * as Path from './apiRoutes';

/* Example API calls to json server */
    //Retrieves value from json-server and dispatch an action
export const getApiValue = () => (dispatch) => {
    ApiService.get(Path.TEST)
    .then((response)=>{
        // Save retrieved testvalue to store
        dispatch(Actions.updateState(response.data[0].testvalue));
    })
}

//Updates value in json-server and retrieves the updated value
export const updateApiValue = (value) => (dispatch) => {
    ApiService.patch(Path.TEST_ITEM, {testvalue: value})
    .then(()=>{
        //Retrieve updated item
        dispatch(getApiValue());
    })
}


