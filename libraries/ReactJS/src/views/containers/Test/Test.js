import React, {Component} from 'react';
import { Container, Card } from 'react-bootstrap';
import { connect } from 'react-redux';
import { testOperations, testSelectors } from './state';
import FormTemplate from '../../components/FormTemplate'; //insert this code

class Test extends Component {
     //componentDidMount() is called after the component has mounted
     componentDidMount(){
        //calls the getApiValue() method defined in mapDispatchToProps
        this.props.getApiValue();
    }
    
    //custom function to handle form submission
    //Pass values from formik form to handleSubmit and call method
    handleSubmit = (value) => {
         //testvalue is the name set on the field's reducer
        this.props.updateApiValue(value.testvalue);  
    }
    render(){
        return(
            <Container>
                <Card>
                    <h1>{this.props.reducerVariable}</h1>
                    <h1>Testttt</h1>
                    <FormTemplate
                            {...this.props}
                            validate={testSelectors.checkIfNumber}
                            handleSubmit={this.handleSubmit}
                            formButtons={[
                                { variant: "success", label: "Save", submit: true }
                                // buttons with [submit: true] will trigger the handleSubmit as defined in FormTemplate.js
                            ]}
                    />
                </Card>
            </Container>
        );
    }
}

const mapStateToProps = (state) =>{
    return{
        formInputs: state.test.testForm,
        reducerVariable: state.test.reducerVariable
    }    
};


const mapDispatchToProps = {
    updateApiValue: testOperations.updateApiValue,
    getApiValue: testOperations.getApiValue
};

export default connect(mapStateToProps, mapDispatchToProps)(Test);
