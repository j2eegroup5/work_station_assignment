import Test from "./Test";
    
export const TestRoutes = [
    {
        label: "Test",
        path: "/Test",
        component: Test,
        exact: true,
        showNav: true //if true, displays the label in the mainHeader.js component
    }
]

