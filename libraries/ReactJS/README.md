# ReactJS Codebase
This codebase includes the following packages:
- [React Bootstrap](https://react-bootstrap.github.io/)
- [React Router DOM](https://reacttraining.com/react-router/web/guides/quick-start)
- [Redux](https://redux.js.org/basics/usagewithreact)
- [Redux Thunk](https://github.com/reduxjs/redux-thunk)
- [Redux Saga](https://redux-saga.js.org/) 
- [Axios](https://github.com/axios/axios)
- [Webpack](https://webpack.js.org/)

## Getting Started
### Prerequisite
##### Node.js
 If you don't have nodejs installed, visit https://nodejs.org and download the latest version.  
 Node Package Manager (npm) will be used to manage all dependencies and packages to be used in the project.

##### Visual Studio Code
Any text editor / IDE will do but the team used Visual Studio Code for the source code editing.  
Download link: https://code.visualstudio.com/download


### Installation

Here is a step-by-step instruction to get the codebase running in your local system.  

1. Open any text editor (preferably Visual Studio Code) and open project folder.
2. Shortcut keys   **Ctrl +  `**  will open Integrated Terminal. (Only for Visual Studio Code)
__Alternative__: Open a terminal (command line) on the project folder directory

    ```
    cd ReactJS
    npm install
    ```
    `npm install` will download all dependencies and packages of the project defined on the **package.json** file.


### Demo  
**Note:** Use `bash` command line to run the application.
1. Run the codebase
    Open a terminal on your project folder (inside ReactJS folder)  
    For running the app using the **Development** configuration:
    ```
    npm run start-dev
    ```
    For running the app using the **Production** configuration:  
    ```
    npm run start-prod
    ```
    
2. Run the JSON-SERVER for REST API calls

    Open a terminal on your project folder (inside ReactJS folder)  

    **Development** Configuration  
    ```
     json-server --watch db.json --port 3030
    ```
    **PRODUCTION** Configuration  
    ```
     json-server --watch db.json --port 5050
    ```
3. Open a web browser (Chrome, Firefox, etc.) and you can now access the codebase on the following:  
**Development** - `http://localhost:3000/`  
**Production** - `http://localhost:5000/`  

4. You can login using the following credentials:  
    username: `demo@test.com` or `test@test.com`  
    password: `demo`  

### Error
```
'env' is not recognized as an internal or external command, operable program or batch file. 
```
* If ever you encounter this kind of error, use **`bash`** command line in running the application instead of `Powershell`

### Build
1. Open a terminal on your project folder (inside ReactJS folder)  
2. Run the following command:  
    **Development** Build:  
    ```
     npm run build-dev
    ```
    **Production** Build:  
    ```
     npm run build-prod
    ```
    
##### That's it! You're now set to customize your own application. Check [this](./md/demo) to find out how to add your first module.  


---

## React Bootstrap

* React-Bootstrap is a complete re-implementation of the Bootstrap components using React. It is a front-end framework for faster and easier web development that includes design templates for forms, buttons, tables, navigation, image carousel, modals and more.  
  
* React-bootstrap was used in this code base for faster development and for the responsiveness of the web pages. 
* Official documentation: [React Bootstrap](https://react-bootstrap.github.io/)

---
  
## React Router DOM

* React Router is a collection of navigational components that compose declaratively with react applications. This package handles all the routing and page transition within the react application.  
  
* Official documentation: [React Router DOM for Web](https://reacttraining.com/react-router/web/guides/quick-start)

---  

## Redux

* Redux is a small library with a simple, limited API designed to be a predictable container for application state. It operates in a similar fashion to a reducing function, a functional programming concept.  

* It helps you write applications that behave consistently, run in different environments (client, server, and native), and are easy to test. 

* Redux provides a solid, stable and mature solution to managing state in your React application. Through a handful of small, useful patterns, Redux can transform your application from a total mess of confusing and scattered state, into a delightfully organized, easy to understand modern JavaScript powerhouse.
* Official doucmentation: [Redux Official Documentation](https://redux.js.org/)

---

## Redux Thunk
* Redux Thunk middleware allows you to write action creators that return a function instead of an action. The thunk can be used to delay the dispatch of an action, or to dispatch only if a certain condition is met. The inner function receives the store methods dispatch and getState as parameters.

* Documentation: [Redux Thunk](https://github.com/reduxjs/redux-thunk)

---

## Redux Saga

* Redux Saga is a library that aims to make application side effects (i.e. asynchronous things like data fetching and impure things like accessing the browser cache) easier to manage, more efficient to execute, easy to test, and better at handling failures.

* A `saga` is like a separate thread in your application that's solely responsible for side effects. redux-saga is a redux middleware, which means this thread can be started, paused and cancelled from the main application with normal redux actions, it has access to the full redux application state and it can dispatch redux actions as well.

* Official documentation: [Redux Saga](https://redux-saga.js.org/)

---

## Axios
* Promise based HTTP client for the browser and node.js
* Axios was used for REST API Calls with the server
* Documentation: [Axios](https://github.com/axios/axios)