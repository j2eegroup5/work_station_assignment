## LLD: Demo (Redux State)

### Filename: `types.js`

##### Constants
* GET_LIST - action creator type to retrieve list ("demo/GET_LIST")
* ADD - action creator type to add data ("demo/ADD")
* EDIT - action creator type to edit data ("demo/EDIT")
* DELETE - action creator type to delete data ("demo/DELETE")
* GET_USER_MESSAGES - action creator type to retrieve messages list ("demo/GET_USER_MESSAGES")

