## LLD: Demo (Redux State)

### Filename: `operations.js`


##### Methods

*  #### **`fetchDefault`** - method to retrieve list from server 

    **Parameters**
    * dispatch
    
    **Flow/Pseudocode**
    * Call API get method to "/users"
	* Get API response
	* dispatch `fetchDefault` action creator with the response.data as parameter


*  #### **`addDemo`** - method to add record to server 

    **Parameters**
    * payload
    * dispatch
    
    **Flow/Pseudocode**
    * Call API post method to "/users" with payload as additional parameter
	* dispatch `fetchDefault` method from the same file


*  #### **`editDemo`** - method to update record to server 

    **Parameters**
    * payload
    * dispatch
    
    **Flow/Pseudocode**
    * Call API patch method to "/users/:id" with payload as additional parameter
	* dispatch `fetchDefault` method from the same file

*  #### **`deleteDemo`** - method to delete record to server 

    **Parameters**
    * id
    * dispatch
    
    **Flow/Pseudocode**
    * Call API delete method to "/users/:id"
	* Get API response
	* dispatch `fetchDefault` method from the same file

*  #### **`sendUserMessage`** - method to add message record to server 

    **Parameters**
    * payload
    
    **Flow/Pseudocode**
    * Call API post method to "/messages" with payload as additional parameter
	* Get API response
	* prompt "Message sent" message


*  #### **`getUserMessages`** - method to retrieve user messages list from server 

    **Parameters**
    * id
    * dispatch
    
    **Flow/Pseudocode**
    * Call API get method to "/messages?userId=:id" 
	* Get API response
	* dispatch `getUserMessages` action creator with the response.data as parameter


*  #### **`clearUserMessages`** - method to clear user messages list from redux store 

    **Parameters**
    * dispatch
    
    **Flow/Pseudocode**
	* dispatch `getUserMessages` action creator [] as parameter