## LLD: Demo (Redux State)

### Filename: `selectors.js`


##### Methods

*  #### **`selectFromList`** - selects specific data from the state based on ID
	**Return**	 
    * state object
    
    **Parameters**
    * state
    * id
    
    **Flow/Pseudocode**
    * Filter state where state id === id
    * Return filtered object

    
*  #### **`getFullName`** - returns full name  
	**Return**	 
    * string
    
    **Parameters**
    * state
    
    **Flow/Pseudocode**
    * Set a variable to the state.firstname value + state.lastname value
    * Return the variable

*  #### **`validateMessage`** - validates message input  
	**Return**	 
    * string
    
    **Parameters**
    * payload
    
    **Flow/Pseudocode**
    * Set a variable to ""
    * IF payload id value is empty, concatenate variable with "Please select a user" string
    * IF payload message value is empty, concatenate variable with "Message field is empty" string
    * Return variable


*  #### **`validate`** - validates add record input  
	**Return**	 
    * string
    
    **Parameters**
    * state
    * payload
    
    **Flow/Pseudocode**
    * Set a variable to ""
    * IF payload id exists in the state, concatenate variable with "ID already exists" string
    * IF payload id value is empty, concatenate variable with "ID field is empty" string
    * IF payload firstname value is empty, concatenate variable with "First name field is empty" string
    * IF payload lastname value is empty, concatenate variable with "Last name field is empty" string
    * IF payload username value is empty, concatenate variable with "Username field is empty" string
    * Return variable