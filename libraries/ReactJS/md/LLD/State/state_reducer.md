## LLD: Demo (Redux State)

### Filename: `reducers.js`

##### Member Variables
* defaultState - main reducer object containing the module redux states
	* userMessages - array list that contains the selected row data messages
	* tableHeader - array that contains the table header names


##### Methods
* reducer - handles state changes upon dispatched actions  
	**Return**	 
    * Redux State
    
    **Parameters**
    * state
    * action
    
    **Flow/Pseudocode**
    * Check dispatched action type 
	* IF action type === GET_LIST
		* return state with `list` state updated with the received payload
	* IF action type === GET_USER_MESSAGES 
		* return state with `userMessages` state updated with the received payload
	* ELSE
		* return state  