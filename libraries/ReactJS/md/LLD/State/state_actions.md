## LLD: Demo (Redux State)

### Filename: `actions.js`


##### Methods

*  #### **`fetchDefault`** - action creator to retreive list
	**Return**	 
    * type : GET_LIST
    * payload : data
    
    **Parameters**
    * data
    
*  #### **`getUserMessages`** - action creator to retreive list
	**Return**	 
    * type : GET_USER_MESSAGES
    * payload : data
    
    **Parameters**
    * data