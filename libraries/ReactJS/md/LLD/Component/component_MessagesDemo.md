## LLD: Demo (Container Components)

### Filename: `MessagesDemo.js`

**Class name: MessagesDemo**

* #### `renderMessages` - renders selected row messages 
	
    **Return**
    * Message component
    
	**Flow/Pseudocode**  
	* Map `userMessages` state props
    * Render Message component based on prop value


*  #### **`mapStateToProps`** - returns redux states
	**Parameters**
    * Redux State
    
    **Return**  
    * userMessages reducer state
    
