## LLD: Demo (Container Components)

### Filename: `index.js`

**Class name: Demo**

* #### `componentDidMount` - call after the component mounted  

	**Flow/Pseudocode**  
	* Dispatch action `fetchDefault` from props  


* #### `toggleAddUser` - show add user input and hide other forms
    **Flow/Pseudocode**  
    * Set `showAddUser` state to NOT `showAddUser` state
    * Set `showAddMessage` state to false
    * Set `showMessages` state to false


*  #### `toggleMessages`  - show messages list and hide other forms

    **Flow/Pseudocode**  
    * Dispatch action `clearUserMessages` from props  
    * Set `showMessages` state to NOT `showMessages` state
    * Set `showAddMessage` state to false
    * Set `showAddUser` state to false


*  #### **`toggleAddMessage`**  - show add message input and hide other forms

    **Flow/Pseudocode**  
    * Dispatch action `clearUserMessages` from props  
    * Set `showAddMessage` state to NOT `showAddMessage` state
    * Set `showMessages` state to false
    * Set `showAddUser` state to false


*  #### **`renderHeader`**  - renders table headers
	**Return**
    * Table column headers
  	
    **Flow/Pseudocode**  
    * Map `tableHeader` state props
    * Render column header with title props


*  #### **`setEdit`**  - sets state of selected row
	**Parameters**
    * Table row data
  	
    **Flow/Pseudocode**  
    * Set activeId state to the row data id
    * Set editLastName state to row data lastname
    * Set editFirstname to row data firstname
    * Set editUsername to row data username

*  #### **`clearEdit`**  - clears state of selected row
    **Flow/Pseudocode**  
    * Set activeId state to ""
    * Set editLastName state to ""
    * Set editFirstname to row ""
    * Set editUsername to row ""

*  #### **`saveEdit`**   - saves row edit details
    **Flow/Pseudocode**  
    * Define an object variable and set the key values with these state values:  
    	* activeId, editFirstName, editLastName, editUsername 
    * Dispatch action `editDemo` from props with the created object as 
    * Call `clearEdit` method

*  #### **`setMessages`**  - sets selected messages of selected row
	**Parameters**
    * Table row data
    
    **Flow/Pseudocode**  
    * Dispatch action `getUserMessages` from props with the row data id as parameter
    * Set `showMessages` state to true
    * Set `showAddMessage` state to false
    * Set `showAddUser` state to false
    * Set `activeUserMessage` state to row data firstname and lastname

*  #### **`handleInputChange`** - handle input events
	**Parameters**
    * Event
    
    **Flow/Pseudocode**  
    * Set event name state to event value


*  #### **`renderList`**  - renders table list
	**Return**
    * Table Rows
    
    **Flow/Pseudocode**  
    * Check if tableList array props length is greater than 0
    * IF tableList array props length is greater than 0
    	* Map through the tableList array prop   
    	* IF the activeId state is equal to the row data id
    		* Return row with edit input fields (editFirstnam, editLastname, editUsername)
    		* Return row with Save and Cancel buttons	 	 
    	* ELSE
    		* Return row with row data text (firstname, lastname, username)
    		* Return row with Edit, Delete, and Messages buttons
    * ELSE
    	* Return row with "No records found" text  
    	
       
*  #### **`mapStateToProps`** - returns redux states
	**Parameters**
    * Redux State
    
    **Return**  
    * user reducer state
    * isAuthenticated reducer state
    * tableHeader reducer state
    * tableList reducer state
    
    
*  #### *`mapDispatchToProps`* - returns module dispatch actions
    **Return**  
    * module state functions