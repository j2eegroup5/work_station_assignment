## LLD: Demo (Container Components)

### Filename: `SendMessageDemo.js`

**Class name: SendMessageDemo**

*  #### **`handleInputChange`** - handle input events
	**Parameters**
    * Event
    
    **Flow/Pseudocode**  
    * Set event name state to event value

*  #### **`handleSubmit`** - handle submit event

    **Flow/Pseudocode**  
    * Validate state values using the selector `validateMessage` function with the `state` as parameter
    * IF return value of validation is not empty
    	* Set `error` state value to the returned value
    * ELSE
    	* Define an object variable and set the key values with these state values:
    		* Call selector `getFullName` function with the `user` props as parameter to return full name value    
    		* id, fullname, message
        * Dispatch action `sendUserMessage` from props with the created object as parameter
        * Call `toggleAddMessage` method that is passed as props from index	 
        
        
*  #### **`renderOptions`** - renders custom error component
	**Return**
    * select options
    
    **Flow/Pseudocode**  
    * Map `tableList` state props
    * Render option user data props
    
*  #### **`invalidPrompt`** - renders custom error component
	**Return**
    * Component
    
    **Flow/Pseudocode**  
    * If `error` state is not empty
    	* Return error alert component
    * Return " " otherwise  

*  #### **`mapStateToProps`** - returns redux states
	**Parameters**
    * Redux State
    
    **Return**  
    * user reducer user
    * tableList reducer state