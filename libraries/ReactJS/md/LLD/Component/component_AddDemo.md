## LLD: Demo (Container Components)

### Filename: `AddDemo.js`

**Class name: AddDemo**

*  #### **`handleInputChange`** - handle input events
	**Parameters**
    * Event
    
    **Flow/Pseudocode**  
    * Set event name state to event value

*  #### **`handleSubmit`** - handle submit event

    **Flow/Pseudocode**  
    * Validate state values using the selector `validate` function with the `tableList` props and the `state` as parameter
    * IF return value of validation is not empty
    	* Set `error` state value to the returned value
    * ELSE
    	* Define an object variable and set the key values with these state values:  
    		* id, firstname, lastname, username 
        * Dispatch action `addDemo` from props with the created object as parameter
        * Call `toggleAddUser` method that is passed as props from index	  	 

*  #### **`invalidPrompt`** - renders custom error component
	**Return**
    * Component
    
    **Flow/Pseudocode**  
    * If `error` state is not empty
    	* Return error alert component
    * Return " " otherwise  
    
*  #### **`mapStateToProps`** - returns redux states
	**Parameters**
    * Redux State
    
    **Return**  
    * tableList reducer state

    
*  #### *`mapDispatchToProps`* - returns module dispatch actions
    **Return**  
    * addDemo function