## Creating your module's container component (Demo)
Refer [here](../LLD/FileStructure.md) for the folder structure
### Objective  
* Create a container for the demo module
* Implement JSON to Form rendering
* Call state actions on button clicks

##### I. Container Component
1. Create `Test.js` file under `src/views/containers/Test` that will serve as your module's parent container 

     ```javascript
    import React, {Component} from 'react';
    import { Container, Card } from 'react-bootstrap';
    class Test extends Component {
        render(){
            return(
                <Container>
                    <Card>
                        <h1>Test Page</h1>
                    </Card>
                </Container>
            );
        }
    }
    
    export default Test;
    
    ```
2. Create `routes.js` file under `src/views/containers/Test`.
`routes.js` file will hold all your module's routes:
    ```javascript
    import Test from "./Test";
    
    const TestRoutes = [
        {
            label: "Test",
            path: "/Test",
            component: Test,
            exact: true,
            showNav: true //if true, displays the label in the mainHeader.js component
        }
    ]
    
    export default TestRoutes;
    ```
3. Add the module routes on the main modules' route definition under `src/views/containers/Main/PageRoutes.js`
   
    ```javascript
    `... other page imports`
    import TestRoutes from '../Test/routes';
    
    const PageRoutes = [
        ...TestRoutes,
        `... other page routes`
    ];
    
    export default PageRoutes;
    ```

4. Connect the component to redux to have access to the `test` reducer
    ```javascript
    import React, {Component} from 'react';
    import { Container, Card } from 'react-bootstrap';
    import { connect } from 'react-redux';
    
    class Test extends Component {
        render(){
            return(
                <Container>
                    <Card>
                        <h1>Test Page</h1>
                    </Card>
                </Container>
            );
        }
    }
    
    const mapStateToProps = (state) =>{
        return{
            reducerVariable: state.test.reducerVariable,
            formInputs: state.testForm,
        }    
    };
    
    export default connect(mapStateToProps)(Test);
    
    ```
    
5. import the `selectors` and `operations` of the module and define `operations` methods under mapDispatchToProps  
    ```javascript
    import React, {Component} from 'react';
    import { Container, Card } from 'react-bootstrap';
    import { connect } from 'react-redux';
    import { testOperations, testSelectors } from './state';
    
    class Test extends Component {
        render(){
            return(
                <Container>
                    <Card>
                        <h1>Test Page</h1>
                    </Card>
                </Container>
            );
        }
    }
    
    const mapStateToProps = (state) =>{
        return{
            reducerVariable: state.test.reducerVariable,
            formInputs: state.test.testForm,
        }    
    };
    
    
    const mapDispatchToProps = {
        updateApiValue: testOperations.updateApiValue,
        getApiValue: testOperations.getApiValue
    };
    
    export default connect(mapStateToProps, mapDispatchToProps)(Test);
    ```

7. Define functions inside the module before the `render()` method
    ```javascript
    
    //componentDidMount() is called after the component has mounted
    componentDidMount(){
        //calls the getApiValue() method defined in mapDispatchToProps
        this.props.getApiValue();
    }
    
    //custom function to handle form submission
    //Pass values from formik form to handleSubmit and call method
    handleSubmit = (value) => {
         //testvalue is the name set on the field's reducer
        this.props.updateApiValue(value.testvalue);  
    }

    ```

8. Render the Form as defined in the module's reducer file,
    ```javascript
    import React, { Component } from 'react';
    import { connect } from 'react-redux';
    import { Container, Card } from 'react-bootstrap';
    import { testOperations, testSelectors } from './state';
    import FormTemplate from '../../components/FormTemplate'; //insert this code
    
    class Test extends Component {
    
        componentDidMount() {
            this.props.getApiValue();
        }
    
        //Pass values from formik form to handleSubmit and call method
        handleSubmit = (values) => {
            this.props.updateApiValue(values.testvalue);
        }
    
        render() {
            return (
                <Container>
                    <Card>
                        <h1>{this.props.reducerVariable}</h1>
    
                        <FormTemplate
                            {...this.props}
                            validate={testSelectors.checkIfNumber}
                            handleSubmit={this.handleSubmit}
                            formButtons={[
                                { variant: "success", label: "Save", submit: true }
                                // buttons with [submit: true] will trigger the handleSubmit as defined in FormTemplate.js
                            ]}
                        />
                    </Card>
                </Container>
            );
        }
    }
    
    const mapStateToProps = (state) => {
        return {
            reducerVariable: state.test.reducerVariable,
            formInputs: state.test.testForm,
        }
    };
    
    const mapDispatchToProps = {
        updateApiValue: testOperations.updateApiValue,
        getApiValue: testOperations.getApiValue
    };
    
    export default connect(mapStateToProps, mapDispatchToProps)(Test);
    ```

### Save your changes and run the application
### You're done!