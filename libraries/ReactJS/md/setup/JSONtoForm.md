## JSON to Form (Dynamic rendering of forms and tables)

In this codebase, forms and tables are rendered dynamically using template components.  
These presentational components can be found at:  

* Table Template: `src/views/components/TableTemplate.js`  
* Form Template: `src/views/components/FormTemplate.js`  


#### How does it work?
##### 1. Table Template
The `TableTemplate.js` component renders a table, table headers, table body, and row buttons with the use of passed props.  

***Required props:***  
* `tableHeader` - contains the header titles for each columns of the table  
* `tableColumns` - defines the names for the column where the table list data is to be rendered (`name` refers to state name)  
* `tableList` - contains the content of the table body  

***Optional props:***  
* `rowButtons` - render buttons on each row of the table  

***Usage:***
* Define the table props (list, tableHeader and tableColumns) definition in the module's `reducers.js` file.  
```javascript
--- reducers.js

import * as types from "./types";

const defaultState = {
    
    list: [
      {
        "id": "101",
        "app": "Jinjaguars",
        "company": "Jenga Inc.",
        "description": "Mobile application game"
      },
      {
        "id": "102",
        "app": "TigerMania",
        "company": "Jungle Corp.",
        "description": "Web application game"
      }
    ], //holds the data to be presented on the table
    
    table:{
    	// Table headers
        tableHeader:[
            "ID #",
            "App Name",
            "Company Name",
            "Description",
            ""
        ],
        
        //The array values should match the names or ["keys"] on the list array data.
        tableColumns:[
            "id",
            "app",
            "company",
            "description"
        ]
    },
}
......
```
* Include the reducer values in the `mapStateToProps` in the component where you want to render the table.  
*(Let's say the reducer was  exported as `nameOfReducer`)*
```javascript
...
    const mapStateToProps = (state) =>{
      return{
          tableHeader: state.nameOfReducer.table.tableHeader,
          tableColumns: state.nameOfReducer.table.tableColumns,
          tableList: state.nameOfReducer.list
      }
    };

...
    export default connect(mapStateToProps)(ComponentName);
...
```
* Import the Table Template component in the container component
```javascript
--- ComponentName.js

	import TableTemplate from '../../components/TableTemplate';
```

* Render the Table Template and pass the props to the component
```javascript
	render(){
      return(
        ...other codes
        
        <TableTemplate 
            {...this.props} //pass the props to the component
        />
        
        //Alternative
        <TableTemplate 
            tableHeader = this.props.[propName]
            tableColumns = this.props.[propName]
            tableList = this.props.[propName]
        />
        //End
        ...
      )
    }
```


* (Optional) To add buttons for each row, create a prop definition for `rowButtons` inside the `<TableTemplate/>` component  

	***Required array definition:***  
	* `variant` - react-bootstrap button's color style
	* `label` - text of the button
	* `onClick`  - action when button is clicked
	
```javsacript
	<TableTemplate 
        {...this.props}
        
        // Define rowButtons props
        rowButtons={[
            {variant: "danger", label: "Button 1", onClick: ()=>alert("Clicked Button 1")},
            {variant: "success", label: "Button 2", onClick: ()=>alert("Clicked Button 2")},
            {variant: "warning", label: "Button 3", onClick: ()=>alert("Clicked Button 3")}
        ]}
        
    />
```  
Refer [here](https://react-bootstrap.github.io/components/buttons/) for other button styles.  

##### Refresh the page. You're table is ready!  


##### 2. Form Template  
The `FormTemplate.js` component renders a form based on the passed props.  
Currently the template's layout only handles single column form.  

***Required Props***   
(Basic Form)
* `formInputs` - definition of the reducer form input values  
* `formButtons` - definition of buttons to be rendered

(Tabbed Form)
* `tabbed` - defines the form as multi-tabbed form
* `tabbedInputs` - definition of the reducer form tabbed input values  

*Available fields:*  
* Text (Plain, Email, Password)
* Textarea
* Number
* Select
* Radio Button
* Checkbox 
* Date Picker
* File Upload

***Usage***  
##### a) Basic Form
* Define the form inputs in the module's `reducer.js` file.  

```javascript
...

const defaultState = {
	inputForm:[
        {
            type: "text",
            label: "App Name",
            placeholder: "Application Name",
            maxLength: 125,
            name: "app"
        },
        {
            type: "text",
            label: "Company Name",
            placeholder: "Company Name",
            maxLength: 125,
            name: "company"
        },
        {
            component: "textarea", //component is used for textarea instead of type
            label: "Description",
            placeholder: "Description",
            maxLength: 250,
            name: "description"
        }
    ],
}
```
`NOTE`: You can declare multiple forms in your reducer. Make sure that they are grouped according to what form it belong.  

* Include the reducer form values in the mapStateToProps in the component where you want to render the form.  
*(Let's say the reducer was exported as nameOfReducer)*
```javascript
...
    const mapStateToProps = (state) =>{
      return{
          formInputs: state.nameOfReducer.inputForm
      }
    };

...
    export default connect(mapStateToProps)(ComponentName);
...
```

* Import the Form Template component in the container component
```javascript
--- ComponentName.js

	import FormTemplate from '../../components/FormTemplate';
```  

* Define the `handleSubmit` method before the render() method
```javascript
    handleSubmit = (values) => {
        //insert your submit method
        `values` contains the form key[name] values
        console.log(values)
    }
```

* Render the Form Template and pass the `props` and `handleSubmit` to the component
```javascript
	render(){
      return(
        ...other codes
        
        <FormTemplate 
            {...this.props} //pass the props to the component
            handleSubmit={this.handleSubmit} //pass handleInputChange as props
        />
        
        //Alternative
        <FormTemplate 
            formInputs = {this.props.formInputs} //pass the forminput to the component
            handleSubmit={this.handleSubmit} //pass handleInputChange as props
        />
        ...
      )
    }
```

* Define the form's buttons and pass it as `formButtons` props  
***Required array definition:***  
	* `variant` - react-bootstrap button's color style
	* `label` - text of the button
	* `onClick`  - action when button is clicked  
	* `submit` - if set to `true`, this will trigger the handleSubmit() method
	
	***Optional array definition:***  
	* `className` - custom style for the button
	
```javascript
	render(){
      return(
        ...other codes
        
        <FormTemplate 
            {...this.props}
            handleSubmit={this.handleSubmit}
            formButtons={[
                {variant: "success", label: "Save", className:"pull-right", submit: true}, //set as submit trigger
                {variant: "danger", label: "Cancel", className:"pull-right", onClick: ()=>alert("Clicked Cancel") }
            ]}
            
            //"pull-right" will place the button to the right-most area of the form
        />
        
        ...
      )
    }
```
##### b) Tabbed Form
* Define the form inputs in the module's `reducer.js` file.  

```javascript
...

const defaultState = {
	tabbedForm: [
        {
            //Label of the Tab
            label: "Tab One",   
            //"form" props contains the form definition found inside the tab
            form: [             
                {
                    type: "text",
                    label: "Tab One Input 1",
                    name: "tabOneInput1",
                    default: ''
                },
                {
                    type: "text",
                    label: "Tab One Input 2",
                    name: "tabOneInput2",
                    default: ''
                }
            ]
        },
        {
            label: "Tab Two",
            form: [
                {
                    type: "text",
                    label: "Tab Two Input 1",
                    name: "tabTwoInput1",
                    default: ''
                },
                {
                    type: "text",
                    label: "Tab Two Input 2",
                    name: "tabTwoInput2",
                    default: ''
                }
            ]
        },
        {
            label: "Tab Three",
            form: [
                {
                    type: "select",
                    label: "Select Input",
                    name: "tabThreeInput1",
                    default: '',
                    placeholder: "Select Item . . ."
                },
                {
                    type: "datepicker",
                    label: "Select Date",
                    name: "tabThreeInput2",
                    default: 1
                }
            ]
        }
    ],
}
```
* Include the reducer form values in the mapStateToProps in the component where you want to render the form.  
NOTE: `tabbedInputs` props is used for tabbed form

*(Let's say the reducer was exported as nameOfReducer)*
```javascript
...
    const mapStateToProps = (state) =>{
      return{
          tabbedInputs: state.nameOfReducer.tabbedForm
      }
    };

...
    export default connect(mapStateToProps)(ComponentName);
...
```

* Import the Form Template component in the container component
```javascript
--- ComponentName.js

	import FormTemplate from '../../components/FormTemplate';
```  

* Define the `handleSubmit` method before the render() method
```javascript
    handleSubmit = (values) => {
        //insert your submit method
        `values` contains the form key[name] values
        console.log(values)
    }
```

* Render the Form Template and pass the `props` and `handleSubmit` to the component
```javascript
	render(){
      return(
        ...other codes
        
        <FormTemplate 
            tabbed={true} //set "tabbed" props to TRUE
            {...this.props} //pass the props to the component
            handleSubmit={this.handleSubmit} //pass handleInputChange as props
        />
        
        //Alternative
        <FormTemplate 
            tabbed={true}
            tabbedInputs = {this.props.tabbedInputs} //pass the tabbedInputs to the component
            handleSubmit={this.handleSubmit} //pass handleInputChange as props
        />
        ...
      )
    }
```

* Define the form's buttons and pass it as `formButtons` props  
***Required array definition:***  
	* `variant` - react-bootstrap button's color style
	* `label` - text of the button
	* `onClick`  - action when button is clicked  
	* `submit` - if set to `true`, this will trigger the handleSubmit() method
	
	***Optional array definition:***  
	* `className` - custom style for the button
	
```javascript
	render(){
      return(
        ...other codes
        
        <FormTemplate 
            {...this.props}
            handleSubmit={this.handleSubmit}
            formButtons={[
                {variant: "success", label: "Save", className:"pull-right", submit: true}, //set as submit trigger
                {variant: "danger", label: "Cancel", className:"pull-right", onClick: ()=>alert("Clicked Cancel") }
            ]}
            
            //"pull-right" will place the button to the right-most area of the form
        />
        
        ...
      )
    }
```
##### Refresh the page. You're form is ready!  