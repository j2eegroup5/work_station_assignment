## Nested Container and Components

The directory under `src/views` is divided into two subfolders:  
* components - contains all common presentational components used throughout the app (renders based on passed props only)  
* containers - smart components (connected with redux). Divided per module.  


#### Module with Nested Component  
**Scenario:** Let's say a module consists of an image slider and three (3) tabs having different form inputs per tabs that is submitted under one form submission.  

Here is the proposed file directory to handle these nested components:
```
/views
	/components
    /containers
    ----/ModuleName
    ---------/CustomComponents
    ---------/TabOne
    --------------/index.js
    --------------/ *.js
    --------------/ *.js
    ---------/TabTwo
    --------------/ * similar to TabOne
    ---------/TabThree
    --------------/ * similar to TabOne
    ---------/SliderPage.js
    ---------/index.js    
```

**Definition:**  
`ModuleName` - parent folder of the module's containers  
`CustomComponents` - contains all custom components that will be used by this module only  
`TabOne` - contains all sub containers inside the first tab  
`TabOne/index.js` - main container for the `TabOne`  
`TabOne/ *.js` - subcontainer file under `TabOne`  
`TabTwo` - contains all sub containers inside the second tab  
`TabThree` - contains all sub containers inside the third tab  
`SliderPage.js` - slider page container  
`index.js` - main container for the module  


#### Handling Multiple Form Inputs  
Here are the steps to reproduce to handle multiple form inputs in each tabs of the container and to submit all forms under one submission.  

1. Declare default state values under the constructor of the module's `index.js` file   
*It is **required** to define the default value to remove the warning for uncontrolled inputs.*  

**Example:** 
```javascript
	constructor(props) {
        super(props)
        this.state={
            //TabOne form inputs
            id: "",
            firstname: "",
            middlename: "",
            lastname: "",
            email: "",      
            
            //TabTwo form inputs
            level: "",
            school: "",
            educ_address: "",
            course: "",
            awards: "",
            
            //TabThree form inputs
            company: "",
            position: "",
            responsibility: "",
            reason: ""
        }
    }
```

`NOTE:` Make sure that the state name of the input defined in the `index.js` constructor is the same with the input field definition in the `reducers.js` file. Click [here](./JSONtoForm.md) to know more.  



2. Define `handleInputChange` method to handle form inputs
```javascript
	handleInputChange = (event) => {
        this.setState({ [event.target.name]: event.target.value });
    }
```

3. Pass the `state` and `handleInputChange` function as props to the tab containers  
```javascript
  <Tabs defaultActiveKey={1} animation={false}>
  
      <Tab eventKey={1} title="Personal Info">
          <TabOne 
          	{...this.state} 
            handleInputChange={this.handleInputChange} 
          />
      </Tab>
      
      <Tab eventKey={2} title="Educational Background">
          <TabTwo
          	{...this.state} 
            handleInputChange={this.handleInputChange} 
          />
      </Tab>
      
      <Tab eventKey={3} title="Work Experiance">
          <TabThree
          	{...this.state} 
            handleInputChange={this.handleInputChange} 
          />
      </Tab>
  </Tabs>
```

4. Use the passed props to render the form using the Form Template Component.  
(Continue to pass the props until it reaches the component that the form inputs are rendered in the Tabs subcontainers.)   

	*The `state` on the index file will be passed to the component to be rendered. Every changes will be handled by the `handleInputChange` which will update the value of the state in the `index.js` file.*  

	**Example:**  
```javascript
	<FormTemplate 
        {...this.props} //state value is included here
        handleInputChange={this.props.handleInputChange} //sets input change handler
        className={"m-top-20"}
    />
```
	
   * Refer [here](./JSONtoForm.md) to know how to render Forms and Tables dynamically  

5. Create a function named `handleSubmit` that will define what will happen after the form submission in the `index.js` file  
```
    handleSubmit = () => {
        //do something here
        console.log(this.state) //check updated state value
    }
```

6. Define a button that will trigger the form submission in the `index.js` file  
```
	<Button bsStyle="success" onClick={this.handleSubmit}>
		Save Changes
    </Button>
```




