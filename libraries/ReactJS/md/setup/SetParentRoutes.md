## Setting the application's Parent Routes
###### The parent routes in this base consists of the following:
* Default / Landing Page
* Login Page
* Main Page
    * Contains the app's main modules components    
* Error 404 Page

Refer [here](../LLD/FileStructure.md) for the folder structure

## Setup
Let's assume that you have created your module's container component under `src/views/containers` folder.
##### 1. Define the path for your route under `src/routes/constants.js`

```javascript
export const SAMPLE_PATH = "/samplepath";
```

##### 2. import your component under `src/routes/pages.js`

```javascript
...other imports
import SampleComponent from '../views/containers/SampleComponent';

export {
    ...other exports
    SampleComponent
};
```

##### 3. Define your route inside `<Switch></Switch>` on `src/routes/index.js`

NOTES: 
- If your page component requires authentication to be accessed, use the `<PrivateRoute>` tag, otherwise use `<Route>` tag.
- If you want your parent route to be `nested`, DO NOT use the `exact` keyword inside your <Route> tag.
    - Example: Parent route is `/samplepath`
    - If you do not declare the `exact` keyword, you can render subroutes using `/samplepath` like `/samplepath/moduleOne` and `/samplepath/moduleTwo`
    - However, you should define the definition of these subroutes inside your module's component. Check the [Main Page]((../LLD/FileStructure.md)) container to know how subroutes for the main modules of the codebase were implemented.
```javascript
    class Routes extends Component {
        render(){
            return (
                <MemoryRouter initialEntries={[this.props.currentPath]}>
                    <Switch>
                        {/* Main Routes */}
                        <Route exact path={Path.DEFAULT} component={(Page.Default)}/>
                        <Route exact path={Path.LOGIN} component={(Page.Login)}/>
                        <PrivateRoute path={Path.MAIN} component={(Page.Main)}/>
                        
                        //Inserted code
                        <PrivateRoute path={Path.SAMPLE_PATH} component={(Page.SampleComponent)}/>
                        
                        {/* Error Routes */}
                        <Route component={(Page.Error404)}/>
                    </Switch>
                </MemoryRouter>
        ...
```
    
    
#### You can now access your page using the parent route you've set up.
#### Check this [link](https://reacttraining.com/react-router/web/guides/quick-start) to learn more about React Router DOM
